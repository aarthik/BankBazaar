package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class hooks extends SeMethods{
	@Before
	public void beforeCucumber(Scenario sc)
	{
		startResult();
		testCaseName = sc.getName();
		testCaseDescription =sc.getId();
		category = "Smoke";
		author= "Babu";
		startTestCase();
		startApp("chrome","http://leaftaps.com/opentaps/");
		
	/*System.out.println("Scenario name: "+sc.getName());
		System.out.println("Scenario ID is: " +sc.getId());*/
		
		
		
	}
	@After
	public void afterCucumber(Scenario sc)
	{
		
		closeAllBrowsers();
		stopResult();
		/*System.out.println("Scenario status: "+sc.getStatus());*/
	}

}
