Feature: Create a Lead in Leaftap application
Background:
Given Enter the username as DemoSalesManager
And Enter the password as crmsfa
And click on Login button 
And click on crmsfa link
And click on Leads

#Scenario: Positive flow to Create a lead
#And enter the company name as TESTTECH
#And enter the first name as Kaviya
#And enter the last name as Madhavan
#When onClickingCreate Lead button
#Then Verify whether Lead is created


Scenario Outline: Positive flow to Create a lead
And click on CreateLead
And enter the company name as <companyName>
And enter the first name as <firstname>
And enter the last name as <lastname>
When onClickingCreate Lead button
Then Verify whether Lead is created

Examples:
|companyName|firstname|lastname|
|softtechie1|vivek|karthick|
|techiesol|arun|karthick|
