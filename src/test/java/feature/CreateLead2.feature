Feature: Create a Lead in Leaftap application
Background:
Given Login to Leaftaps application
And click on crmsfa link
And click on Create Lead


#Scenario: Positive flow to Create a lead
#And enter the company name as TESTTECH
#And enter the first name as Kaviya
#And enter the last name as Madhavan
#When onClickingCreate Lead button
#Then Verify whether Lead is created


Scenario Outline: Negative flow to Create a lead
And enter the company name as <companyName>
And enter the first name as <firstname>
And enter the last name as <lastname>
When onClickingCreate Lead button
Then Verify whether Lead is created

Examples:
|companyName|firstname|lastname|
|softtech|kavin1|karthick|
|techiesol||kavi|
