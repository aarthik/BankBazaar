package testcases_bankbazaar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import pages_bankbazaar.bankbazaar_firstpage;
import wdMethods.ProjectMethods;

public class bankbazaar_testcase extends ProjectMethods{
	
	@BeforeClass
	public void setData() {
		testCaseName = "bankbazzar";
		testCaseDescription ="to get the schemes";
		category = "Smoke";
		author= "Aarthi";
		//dataSheetName="TC001";
}

	@Test
	public  void bankbazzarschemes() throws InterruptedException   {
		new bankbazaar_firstpage()
		.mousehoverInvestment()
		.clickMutualFunds()
		.clickSearchforMutualFunds()
		.clickAge()
		.clickMonthAndYear()
		.clickDate()
		.verifyBirthday()
		.clickContinue()
		.clickSalary()
		.clickContinue()
		.clickBank()
		//.clickContinue()
		.typeFirstName("Aarthi")
		.clickViewMutualFunds()
		.getScehemeNames();
		
		
	}
}