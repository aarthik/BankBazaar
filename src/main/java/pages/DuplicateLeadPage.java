package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods{

		public ViewLeadPage clickCreateLead() {
		WebElement eleCreateLead= locateElement("name", "submitButton");
		click(eleCreateLead);
		return new ViewLeadPage(); 
	}
	
	public DuplicateLeadPage getTitleofThePage() {
		getTitle();
		return this;
	}
	
	public DuplicateLeadPage verifyTitleofThePage(String title) {
		verifyTitle(title);
		return this;
	}
				
	
	
	
	
	
}









