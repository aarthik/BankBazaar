package pages;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	
	public OpentapsCRMPage clickEdit() {
		WebElement eleEditbutton = locateElement("linktext", "Edit");
		click(eleEditbutton);
		return new OpentapsCRMPage();
	}
	
	public MyLeadsPage clickDelete() {
		WebElement eleDeletebutton = locateElement("linktext", "Delete");
		click(eleDeletebutton);
		return new MyLeadsPage();
	}
	
	public DuplicateLeadPage clickDuplicateLead() {
		WebElement eleDuplicateLeadbutton = locateElement("linktext", "Duplicate Lead");
		click(eleDuplicateLeadbutton);
		return new DuplicateLeadPage();
	}
	
	@Then("Verify whether Lead is created")
	public ViewLeadPage getLeadFirstNameAndVerify() {
		WebElement eleLeadFirstName = locateElement("id", "viewLead_firstName_sp");
		System.out.println("First name of the lead in View Lead Page is: "+eleLeadFirstName.getText());
		//verifyExactText(eleLeadFirstName, FirstLeadName);
		return this;
	}
	public ViewLeadPage verifyFirstName() {
		WebElement eleLeadFirstName = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(eleLeadFirstName, FirstLeadName);
		return this;
	}
	
	public FindLeadsPage clickFindLeads() throws InterruptedException {
		WebElement eleFindLeads = locateElement("linktext", "Find Leads");
		click(eleFindLeads);
		return new FindLeadsPage();
	}	
		
}