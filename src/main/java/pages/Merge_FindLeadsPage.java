package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class Merge_FindLeadsPage extends ProjectMethods{

	//public String Firstrecord;
	
		
		public Merge_FindLeadsPage typeFirstName(String data) {
			WebElement eleFirstName = locateElement("name", "firstName");
			type(eleFirstName,data);
			return this;
		}
		
			public Merge_FindLeadsPage clickFindLeads() throws InterruptedException {
			WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
			click(eleFindLeads);
			Thread.sleep(2000);
			return this;
		}	
		public MergeLeadsPage clickFirstLead(int index) {
			WebElement eleFirstLead = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	//	String Merge_Firstrecord = eleFirstLead.getText();
			click(eleFirstLead);
			switchToWindow(index);
			return new MergeLeadsPage() ;
		}
		
		public Merge_FindLeadsPage getFirstLeadID() {
			WebElement eleFirstLead = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
			Merge_Firstrecord = eleFirstLead.getText();
			return this;
		}
	
		
		public Merge_FindLeadsPage GetFirstLeadname() {
			WebElement eleFirstLeadName = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a[1]");
		String FirstLeadName = eleFirstLeadName.getText();
		System.out.println(FirstLeadName);
				return this;
		}	
	
}









